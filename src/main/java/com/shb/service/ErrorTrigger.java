package com.shb.service;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.springframework.stereotype.Component;

import java.net.ConnectException;

@Component
public class ErrorTrigger implements Processor {

  private int called = 1;

  @Override
  public void process(Exchange exchange) throws Exception {

    if ((called++ % 4) != 0)
    {
      throw new ConnectException("Cannot connect to the something");
    }
  }
}
